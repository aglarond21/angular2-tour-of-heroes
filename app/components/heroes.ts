import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';

import { Hero } from './../models/hero';
import { HeroService } from './../services/hero';
import { HeroDetailComponent } from './hero-detail';

@Component({
    selector: 'my-heroes',
    templateUrl: 'app/templates/heroes.html',
    styleUrls: ['app/styles/heroes.css'],
    directives: [
        HeroDetailComponent
    ],
    providers: [
    ]
})

export class HeroesComponent implements OnInit {
    title = 'Tour of Heroes';
    heroes: Hero[];
    selectedHero: Hero;

    constructor(
        private router: Router,
        private heroService: HeroService
    ) { }

    onSelect(hero: Hero) {
        this.selectedHero = hero;
    }

    getHeroes() {
        this.heroService.getHeroes().then(heroes => this.heroes = heroes);
    }

    ngOnInit() {
        this.getHeroes();
    }

    gotoDetail() {
        this.router.navigate(['HeroDetail', {id: this.selectedHero.id}]);
    }
}
